/*************************************************************************
 * Java8 Functional Programming Webinar at Sunbeam Infotech.
 * Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 * Date: 16-May-2020 & 17-May-2020
 * Demo: Invoking default methods
 *************************************************************************/

package demo03;

interface Displayable {
	default void show() {
		System.out.println("Displayble show().");
	}
}

interface Printable {
	default void show() {
		System.out.println("Printable show().");
	}
}

class SuperClass implements Displayable {
	@Override
	public void show() {
		System.out.println("SuperClass show()");
	}
}

class SubClass extends SuperClass implements Printable {
}

public class Demo03Main {
	public static void main(String[] args) {
		new SuperClass().show();
		new SubClass().show();
	}
}

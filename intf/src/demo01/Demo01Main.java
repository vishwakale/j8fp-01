/*************************************************************************
 * Java8 Functional Programming Webinar at Sunbeam Infotech.
 * Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 * Date: 16-May-2020 & 17-May-2020
 * Demo: Default methods
 *************************************************************************/

package demo01;

interface Shape {
	double PI = 3.142;
	double calcArea();
	default double calcPeri() {
		return 0.0;
	}
}

class Square implements Shape {
	private double side;
	public Square(double side) {
		this.side = side;
	}
	@Override
	public double calcArea() {
		return side * side;
	}
	@Override
	public double calcPeri() {
		return 4 * side;
	}
}

class Circle implements Shape {
	private double radius;
	public Circle(double radius) {
		this.radius = radius;
	}
	@Override
	public double calcArea() {
		return PI * radius * radius;
	}
}

public class Demo01Main {
	public static void main(String[] args) {
		Square s = new Square(10);
		System.out.println("Square Area: " + s.calcArea());
		System.out.println("Square Peri: " + s.calcPeri());

		Circle c = new Circle(7);
		System.out.println("Circle Area: " + c.calcArea());
		System.out.println("Circle Peri: " + c.calcPeri());
	}
}

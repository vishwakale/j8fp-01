/*************************************************************************
 * Java8 Functional Programming Webinar at Sunbeam Infotech.
 * Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 * Date: 16-May-2020 & 17-May-2020
 * Demo: Anonymous Inner class to Lambda expressions
 *************************************************************************/

package demo08;

import java.util.Arrays;
import java.util.Comparator;

class Emp {
	private int empno;
	private String ename;
	private double sal;
	private String job;
	public Emp() {
		this(0, "", 0.0, "");
	}
	public Emp(int empno, String ename, double sal, String job) {
		this.empno = empno;
		this.ename = ename;
		this.sal = sal;
		this.job = job;
	}
	public int getEmpno() {
		return empno;
	}
	public void setEmpno(int empno) {
		this.empno = empno;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public double getSal() {
		return sal;
	}
	public void setSal(double sal) {
		this.sal = sal;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	@Override
	public String toString() {
		return "Emp [empno=" + empno + ", ename=" + ename + ", sal=" + sal + ", job=" + job + "]";
	}
}

public class Demo08Main {
	public static void printEmps(Emp[] a) {
		for (Emp e : a)
			System.out.println(e);
		System.out.println();
	}
	public static void main(String[] args) {
		Emp[] arr = new Emp[] {
			new Emp(5, "S", 745, "Clerk"),
			new Emp(9, "D", 834, "Manager"),
			new Emp(7, "K", 345, "Salesman"),
			new Emp(2, "G", 534, "Analyst"),
			new Emp(4, "W", 742, "Manager"),
			new Emp(1, "U", 875, "Manager"),
			new Emp(8, "B", 528, "Salesman"),
			new Emp(3, "N", 656, "Clerk")
		};
	
		// sort by id asc -- local class (anonymous object)
		class EmpNoComparatorAsc implements Comparator<Emp> {
			@Override
			public int compare(Emp e1, Emp e2) {
				return e1.getEmpno() - e2.getEmpno();
			}
		};
		Arrays.sort(arr, new EmpNoComparatorAsc());
		printEmps(arr);
		
		// sort by name asc -- anonymous inner class
		Comparator<Emp> enameComparatorAsc = new Comparator<Emp>() {
			@Override
			public int compare(Emp e1, Emp e2) {
				return e1.getEname().compareTo(e2.getEname());
			}
		};
		Arrays.sort(arr, enameComparatorAsc);
		printEmps(arr);
		
		// sort by name desc -- anonymous inner class (anonymous object)
		Arrays.sort(arr, new Comparator<Emp>() {
			@Override
			public int compare(Emp e1, Emp e2) {
				return -e1.getEname().compareTo(e2.getEname());
			}
		});
		printEmps(arr);
		
		
		
				
		// sort by job asc -- lambda expression
		Arrays.sort(arr, (e1,e2) -> e1.getJob().compareTo(e2.getJob()));
		printEmps(arr);
		
		// sort by sal desc --lambda expression (named object)
		Comparator<Emp> salComapratorDesc = (e1,e2) -> (int)Math.signum(e2.getSal() - e1.getSal());
		Arrays.sort(arr, salComapratorDesc);
		printEmps(arr);
		System.out.println("salComapratorDesc class: " + salComapratorDesc.getClass());
	}
}

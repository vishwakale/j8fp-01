/*************************************************************************
 * Java8 Functional Programming Webinar at Sunbeam Infotech.
 * Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 * Date: 16-May-2020 & 17-May-2020
 * Demo: Default methods - Ambiguity error
 *************************************************************************/

package demo02;

interface Displayable {
	default void show() {
		System.out.println("Displayble show().");
	}
}

interface Printable {
	default void show() {
		System.out.println("Printable show().");
	}
}

//class FirstClass implements Displayable, Printable {
//}

class SecondClass implements Displayable, Printable {
	public void show() {
		System.out.println("SecondClass show().");
	}
}

class ThirdClass implements Displayable {
}

class FourthClass implements Printable {
}

class FifthClass implements Displayable, Printable {
	public void show() {
		//show(); // recursion
		//super.show(); // error - no super class
		System.out.print("Calling Displayable: ");
		Displayable.super.show();
		System.out.print("Calling Printable: ");
		Printable.super.show();
	}
}

public class Demo02Main {
	public static void main(String[] args) {
		new SecondClass().show();
		new ThirdClass().show();
		new FourthClass().show();
		new FifthClass().show();
	}
}

/*************************************************************************
 * Java8 Functional Programming Webinar at Sunbeam Infotech.
 * Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
 * Date: 16-May-2020 & 17-May-2020
 * Demo: Default methods - Interface diamond inheritance
 *************************************************************************/

package demo05;

interface Flyable {
	default void fly() {
		System.out.println(this.getClass().getName() + " - Fly");
	}
}

interface Aeroplane extends Flyable {
//	default void fly() {
//		System.out.println(this.getClass().getName() + " - Fly above clouds.");
//	}
}

interface Bird extends Flyable {
//	default void fly() {
//		System.out.println(this.getClass().getName() + " - Fly below clouds.");
//	}
}

interface AeroplaneBird extends Aeroplane, Bird {
//	default void fly() {
//		System.out.println(this.getClass().getName() + " - Fly whereever.");
//	}
}

class RealAeroplaneBird implements Aeroplane, Bird {
}

public class Demo05Main {
	public static void main(String[] args) {
		new RealAeroplaneBird().fly();
	}
}
